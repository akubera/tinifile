// Author:Andrew Kubera <andrew.michael.kubera@cern.ch> 
// Date: 20/05/2014

/*************************************************************************
 * Copyright (C) 2014, Andrew Kubera                                     *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TIniFile
#define ROOT_TIniFile

//////////////////////////////////////////////////////////////////////////
//
// TIniFile
//
// Class which reads and parses a standard .ini configuration file
//
//
//
//////////////////////////////////////////////////////////////////////////


#ifndef ROOT_TNamed
#include "TNamed.h"
#endif

#include <string>
#include <map>

class TIniFile : public TNamed {
public:
    // Default Constructor
    TIniFile();

    // Construct from filename and optional object name
    TIniFile(const std::string& filename, const char *name = "");

    // Destructor
    virtual ~TIniFile();

protected:
    // Filename the object was given to open
    TString mFilename;

    std::map<std::string,std::map<std::string, std::string> > _kvp;

    ClassDef(TIniFile,1);
};

#endif
