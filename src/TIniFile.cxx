// Author:Andrew Kubera <andrew.michael.kubera@cern.ch>
// Date: 20/05/2014

/*****
 * Copyright (C) 2014
 *
 ****/
///////////
//
// TIniFile
//
// Class which reads and parses a standard .ini configuration file
//
//
//
///////////


#include "TIniFile.h"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <utility>
#include <sstream>
#include <cassert>

ClassImp(TIniFile)

TIniFile::TIniFile()
        : TNamed()
{
    /* Begin_html
     Default constructor
     End_html */
}

TIniFile::TIniFile(const std::string& filename, const char *name)
        : TNamed(name, ""),
          mFilename(filename)
{
    std::ifstream file(filename.c_str());
    if (!file.good()) {
        std::cerr << "Could not open file '" << filename << "'\n";
        return;
    }
    
    int line_count = -1;
    std::string current_heading_name = "";

    const std::unary_negate<std::pointer_to_unary_function<int, int>> not_space(std::not1(std::ptr_fun(&::isspace)));

    while (file.good()) {
        std::string line;
        std::getline(file, line);
        line_count++;
        if (line.size() == 0) {
            continue;
        }
        
        char start_char = line[0];
        std::stringstream ss;
        std::string::reverse_iterator sec_end;

        switch (start_char) {
                // skip comments
            case '#': case ';': case '\n':
                break;
                // begin a new section
            case '[':
                sec_end = std::find(line.rbegin(), line.rend(), ']');
                if (sec_end == line.rend()) {
                    ss << "Malformed ini file, missing closing ']' : " << filename << ':' << line_count;
                    throw ss.str();
                }

                {
                std::string::iterator heading_start = std::find_if(line.begin()+1, sec_end.base(), not_space);
                std::string::iterator heading_stop = std::find_if(sec_end+1, line.rend(), not_space).base();
                current_heading_name = std::string(heading_start, heading_stop);
                }
                                                   
                break;
                // malformed entities
            case '=': case '{':
                ss.clear();
                ss << "Malformed ini file " << filename << ':' << line_count;
                throw ss.str();
            default:
                // if we haven't begun a heading yet - throw an error
                if (current_heading_name == "") {
                    ss << "Malformed ini file " << filename << ':' << line_count;
                    throw ss.str();
                }
                
                // find the equals sign
                std::string::iterator eq = std::find(line.begin(), line.end(),  '=');

                // no '=' in line
                if (eq == line.end()) {
                    // only spaces - ignore the lien
                    if (std::find_if(line.begin(), line.end(), not_space) == line.end()) {
                        continue;
                    }
                    ss << "Malformed ini file, missing equals sign " << filename << ':' << line_count;
                    throw ss.str();
                }
                
                // key start and key end
                std::string::iterator ks = std::find_if(line.begin(), eq, not_space),
                                      ke = std::find_if(ks, eq, ::isspace);
                
                assert(ks != ke);
                assert(std::find_if(ke, eq, not_space) == eq);
                
                // value start and end
                std::string::iterator vs = std::find_if(eq+1, line.end(), not_space),
                                      ve = std::find_if(line.rbegin(), std::string::reverse_iterator(vs), not_space).base();
                //if (vs == ve) {
                //  std::cerr << "vs == ve :: " << line <<'\n';
                //exit(1);
                //}
                
                // Add to the map
                _kvp[current_heading_name][std::string(ks, ke)] = std::string(vs, ve);
                //                _kvp[current_heading_name].emplace(make_pair(ks, ke, vs, ve));
       }
    }
}




TIniFile::~TIniFile()
{
}

