#
# Makefile
#
# genreates the shared object file loadable by root scripts to use the
# TIniFile class
#

ROOTCINT = rootcint
CXX = $(shell root-config --cxx)
CFLAGS = -fPIC $(shell root-config --cflags)
CXX=g++

RM = rm -vf

all: tinifile.so

tinifile.so:  genIniFile.C src/TIniFile.h src/TIniFile.cxx
	@echo "Compiling using root-preferred compiler '${CXX}'"
	${CXX} ${CFLAGS} -c genIniFile.C -o tmp_gen.o
	${CXX} ${CFLAGS} -c src/TIniFile.cxx -o tmp_ini.o

	${CXX} -shared tmp_gen.o tmp_ini.o -o $@


genIniFile.C: src/TIniFile.h
	${ROOTCINT} -f $@ -c $<


clean:
	${RM} *.o
	${RM} *.so
	${RM} tmp*
