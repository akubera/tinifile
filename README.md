# TIniFile

A class for the [ROOT](http://root.cern.ch/) C++ framework which parses and
loads [.ini](http://en.wikipedia.org/wiki/INI_file) files which may contain
settings or run parameters.

## Installation

We will worry about this later, yeah?

## Running

Simply Create the TIniFile object with a filename or ifstream, and the section
separated key-value pairs will be loaded in.

## Example

test.ini

    #
    # Comment
    #
    
    [Section0]
    key=val
    mass=134
    energy=2000

    [Beam]
    state=on
    energy=3e4

Code

    TIniFile ini("test.ini");
    std::cout << ini["Section0"]["mass"] << '\n'; // 134
    std::cout << "Beam is " << ini["Beam"]["state"] << '\n'; // Beam is on

    float beam_energy = atof(ini["Beam"]["energy"]); // beam_energy == 30000


More features will be available.